package com.shootan.stellarpatrol.screens.menu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.shootan.stellarpatrol.CombatScreen;
import com.shootan.stellarpatrol.engine.Base2DScreen;

import static com.shootan.stellarpatrol.lib.Constants.BACKGROUNDS;
import static com.shootan.stellarpatrol.lib.Constants.WORLD_SIZE;

/**
 * Created by Geeksploit on 24.08.2017.
 */

public class MenuScreen extends Base2DScreen {

    public static final String TAG = CombatScreen.class.getName();

    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private Viewport extendViewport;
    private Texture backgroundTexture;

    public MenuScreen(Game game) {
        super(game);
    }

    public Viewport getViewport() {
        return extendViewport;
    }

    @Override
    public void show() {
        super.show();
        spriteBatch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
        extendViewport = new ExtendViewport(WORLD_SIZE, WORLD_SIZE);
        backgroundTexture = BACKGROUNDS.get(MathUtils.random(BACKGROUNDS.size() - 1));
    }

    @Override
    public void resize(int width, int height) {
        extendViewport.update(width, height, true);
    }

    @Override
    public void render (float delta) {
        extendViewport.apply();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(extendViewport.getCamera().combined);
        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, extendViewport.getWorldWidth(), extendViewport.getWorldHeight());
        spriteBatch.end();

    }

    @Override
    public void dispose () {
        shapeRenderer.dispose();
        spriteBatch.dispose();
        backgroundTexture.dispose();
        super.dispose();
    }

    //region Implement InputProcessor interface
    @Override
    public boolean keyDown(int keycode) {
        System.out.printf("%12s: %s\n", Thread.currentThread().getStackTrace()[1].getMethodName(), Input.Keys.toString(keycode));
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        System.out.printf("%12s: %s\n", Thread.currentThread().getStackTrace()[1].getMethodName(), Input.Keys.toString(keycode));
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        System.out.printf("%12s: %s\n", Thread.currentThread().getStackTrace()[1].getMethodName(), character);
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        System.out.printf("%12s:%5dx%5dy\tpointer%2d\tbutton%2d\n", Thread.currentThread().getStackTrace()[1].getMethodName(), screenX, screenY, pointer, button);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        System.out.printf("%12s:%5dx%5dy\tpointer%2d\tbutton%2d\n", Thread.currentThread().getStackTrace()[1].getMethodName(), screenX, screenY, pointer, button);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        System.out.printf("%12s:%5dx%5dy\tpointer%2d\n", Thread.currentThread().getStackTrace()[1].getMethodName(), screenX, screenY, pointer);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        System.out.printf("%12s:%5dx%5dy\n", Thread.currentThread().getStackTrace()[1].getMethodName(), screenX, screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        System.out.printf("%12s:%5d\n", Thread.currentThread().getStackTrace()[1].getMethodName(), amount);
        return false;
    }
    //endregion

}
