package com.shootan.stellarpatrol;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import static com.shootan.stellarpatrol.lib.Constants.BACKGROUNDS;
import static com.shootan.stellarpatrol.lib.Constants.WORLD_SIZE;

/**
 * Created by Geeksploit on 13.08.2017.
 */

public class CombatScreen extends ScreenAdapter {

    public static final String TAG = CombatScreen.class.getName();

    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private Viewport extendViewport;
    private Texture backgroundTexture;

    public Viewport getViewport() {
        return extendViewport;
    }

    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
        extendViewport = new ExtendViewport(WORLD_SIZE, WORLD_SIZE);
        backgroundTexture = BACKGROUNDS.get(MathUtils.random(BACKGROUNDS.size() - 1));
    }

    @Override
    public void resize(int width, int height) {
        extendViewport.update(width, height, true);
    }

    @Override
    public void render (float delta) {
        extendViewport.apply();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(extendViewport.getCamera().combined);
        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, extendViewport.getWorldWidth(), extendViewport.getWorldHeight());
        spriteBatch.end();

    }

    @Override
    public void dispose () {
        shapeRenderer.dispose();
        spriteBatch.dispose();
        backgroundTexture.dispose();
    }
}
