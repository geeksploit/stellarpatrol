package com.shootan.stellarpatrol.lib;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Geeksploit on 14.08.2017.
 */

public class Constants {

    public static final float WORLD_SIZE = 400f;

    public static final List<Texture> BACKGROUNDS = new ArrayList<Texture>() {{
        add(new Texture("background_0.jpg"));
        add(new Texture("background_1.jpg"));
        add(new Texture("background_2.jpg"));
        add(new Texture("background_3.jpg"));
    }};

}
