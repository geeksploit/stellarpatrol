package com.shootan.stellarpatrol;

import com.badlogic.gdx.Game;
import com.shootan.stellarpatrol.screens.menu.MenuScreen;

/**
 * Created by Geeksploit on 13.08.2017.
 */

public class StellarPatrolGame extends Game {

	public static final String TAG = StellarPatrolGame.class.getName();

	@Override
	public void create () {
		setScreen(new MenuScreen(this));
	}

}
